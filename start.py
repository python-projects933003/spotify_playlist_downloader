from convert_csv import Converter
from downloader import Download
from playlist import Playlist

import os

class Start:

    #Inputs username and playlist to converter. Downloads playlist into local folder and creates zip.
    def run(self):
        user,playlist = self.ui()

        converter = Converter(user=user, playlist=playlist)

        converter.make_csv()

        playlist = converter.getPlaylist()

        d = Download(playlist)
        d.search()

    #Shows user input prompts. Only progresses if valid input.
    def ui(self):
        while True:
            user, playlist = self.inputs()
            converter = Converter(user = user, playlist= playlist)
            res = converter.validCred(user, playlist)

            if "Invalid" in res:
                os.system("cls")
                print("Invalid usename or playlist id.")
                continue
            else:
                return user, playlist
                
    #User input prompts method
    def inputs(self):
        user = input("Spotify User Name: ")
        playlist = input("Playlist Id: ")

        return user, playlist