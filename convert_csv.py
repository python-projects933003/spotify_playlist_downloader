#Script that turns spotify playlist into csv file containing track name and the artist.

import csv
import os
import re

import spotipy as sp
from dotenv import load_dotenv
from spotipy.oauth2 import SpotifyClientCredentials
from song import Song
from playlist import Playlist

#Converter class initialises session to spotifies dev api.
#Also creates csv file contaning all songs from playlist provided.
class Converter:
    #Initialises session
    load_dotenv()
    CLIENT_ID = os.getenv("CLIENT_ID", "")
    CLIENT_SECRET = os.getenv("CLIENT_SECRET", "")
    OUTPUT_FILE_NAME = "track_info.csv"
    PLAYLIST_NAME = ""
    client_cred_manager = SpotifyClientCredentials(
        client_id = CLIENT_ID, client_secret = CLIENT_SECRET
    )

    try:
        session = sp.Spotify(client_credentials_manager=client_cred_manager)
        s_list = []
    except:
        raise TypeError("Invalid user or playlist id")

    def __init__(self, user, playlist):
        self.user = user
        self.playlist = playlist
    
    #Checks if username and playlist id are valid or not
    def validCred(self, username, playlist_id):
        try:
            results = self.session.user_playlist_tracks(username, playlist_id)
        except:
            results = "Invalid"
        return results
    
    #function that eliminates the original 100 track limit, allowing playlists of anylength to be converted to CSV.
    def get_playlist_tracks(self, username, playlist_id):
        results = self.session.user_playlist_tracks(username, playlist_id)
        tracks = results['items']
        while results['next']:
            results = self.session.next(result = results)
            tracks.extend(results['items'])
        return tracks
    
    #Creates a csv and writes to csv name and artists of each song on playlist.
    def make_csv(self):
        if os.path.exists(self.OUTPUT_FILE_NAME):
            os.remove(self.OUTPUT_FILE_NAME)
        
        if match:= re.match(r"https://open.spotify.com/playlist/(.*)\?", self.playlist):
            playlist_uri = match.groups()[0]
        else:
            raise ValueError("Expected format: https://open.spotify.com/playlist/...")

        tracks = self.get_playlist_tracks(username=self.user, playlist_id=playlist_uri)
        self.PLAYLIST_NAME = self.session.user_playlist(user = self.user, playlist_id = playlist_uri, fields="name")
        self.PLAYLIST_NAME = self.PLAYLIST_NAME["name"]

        with open(self.OUTPUT_FILE_NAME, "w", encoding="utf-8") as file:
            writer = csv.writer(file)
    
            writer.writerow(["track", "artist"])
    
            for track in tracks:
                name = track["track"]["name"]
        
                artists = ", ".join(
                    [artist["name"] for artist in track["track"]["artists"]]
                )

                s = Song(name, artists)
                self.s_list.append(s)
                writer.writerow([name, artists])
                
    def getPlaylist(self) -> Playlist:
        p = Playlist(self.PLAYLIST_NAME, self.s_list)
        return p
    
        
    
    

    

