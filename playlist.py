from song import Song
import os

#Class to define a playlist. 
#Name attribute saves name of playlist.
#Song attribute song object is created for every song, stored in list of song objects.
class Playlist:
    def __init__(self, name: str, songs: [Song]):
        self.name = name
        self.songs = songs
        
    def getSongs(self):
        return self.songs
    
    def getName(self):
        return self.name
    
    #Makes a folder for songs to be stored, uses name of playlist as folder name.
    def makeFolder(self):
        try:
            os.mkdir(path=self.name)
        except:
            print("File already exists")
        