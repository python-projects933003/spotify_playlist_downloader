from youtubesearchpython import VideosSearch
from pytube import YouTube
from moviepy.editor import *
from playlist import Playlist
import shutil
from os import path

#Download class handles downloading the songs from the playlist and storing the output file as a zip folder
#in the local folder
class Download:
    def __init__(self, playlist: Playlist):
        self.playlist = playlist

    def print(self):
        for song in self.playlist.getSongs():
            print(song.getInfo())
    
    #Search method makes a folder for songs to be stored.
    #Searches all songs within the playlist and gets youtube link to song.
    #Saves link gained to associated song object.
    #Calls download once all song links are gained.
    def search(self):
        self.playlist.makeFolder()
        i = 0
        for song in self.playlist.getSongs():
            info = song.getInfo()
            videoSearch = VideosSearch(info, limit = 1)

            link = videoSearch.result()["result"][0]["link"]
            song.setLink(link)
            print(link + " -- " + str(i))
            i += 1

        self.download()
    
    #Download method downloads mp3's of songs within the playlist and stores them.
    #Gets link, song name from song object and calls create mp3 to download.
    #Once downloaded stores in playlist file created.
    #Created zip once all songs are downloaded in file.
    def download(self):
        p_name = self.playlist.getName()

        for song in self.playlist.getSongs():
            link = song.getLink()
            name = song.getName()

            self.createMp3(link, name)
                
            print(name, "(downloaded)")
            shutil.move(name + ".mp3", p_name + "/" + name + ".mp3")
        
        self.createZip()


    def getFolder(self):
        if path.exists(self.playlist.getName()):
            return self.playlist.getName()

    #Create mp3 downloads mp3 based on link provided, and file name is name of song provided.
    def createMp3(self, link, name):
        yt = YouTube(link)
        stream = yt.streams.filter(only_audio=True).first()
        stream.download(filename=name + ".mp3")

    #Creates zip folder out of playlist folder.
    #Deletes original folder once created.
    def createZip(self):
        folder = self.getFolder()
        src = path.realpath(folder)

        root_dir, tail = path.split(src)
        shutil.make_archive(src,
                            'zip',
                            root_dir,
                            tail)
        
        shutil.rmtree(folder)

    def getZip(self) -> str:
        name = str(self.playlist.getName() + ".zip")
        if name:
            return name
        else:
            print("ERROR")


    