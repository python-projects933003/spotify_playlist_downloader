Dependencies: <br>
 -pip install spotipy <br>
 -pip install python-dotenv <br>
 -pip3 install youtube-search-python <br>
 -pip install pytube <br>
 -pip install moviepy <br>

To start: <br>
    -Run "py main.py"
    <br>
Will ask for Spotify username and the playlist Id of the playlist that is to be downloaded. See below for instructions to find playlist id <br>

How to find playlist Id: <br>
    -Open spotify (Brower or Application) <br>
    -Right click on desired playlist <br>
    -Hover over the share option <br>
    -Click "copy link to playlist" <br>

