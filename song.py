class Song:
    def __init__(self, name, artist):
        self.name = name
        self.artist = artist
        self.link = ''
    
    def getName(self):
        return self.name
    
    def getArtist(self):
        return self.artist
    
    def getInfo(self):
        return str(self.name) + ", " + str(self.artist)
    
    def setLink(self, link):
        self.link = link
        
    def getLink(self):
        return self.link